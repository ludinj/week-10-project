export interface IFluid {
  base64: string;
  aspectRatio: number;
  src: string;
  srcSet: string;
  sizes: string;
}

export interface IChildImageSharp {
  fluid: IFluid;
}

export interface IImage {
  childImageSharp: IChildImageSharp;
}

export interface IFrontmatter {
  title: string;
  image: IImage;
  stack: string;
  slug: string;
  gitlab: string;
  deploy: string;
}

export interface INode {
  frontmatter: IFrontmatter;
}

export interface IEdge {
  node: INode;
}

export interface AllMarkdownRemark {
  edges: IEdge[];
}

export interface IQuery {
  allMarkdownRemark: AllMarkdownRemark;
}

export interface IEducation {
  school: string;
  timeFrame: string;
  topic: string;
}

export interface ChildImageSharp {
  fluid: Fluid;
}

export interface Image {
  childImageSharp: ChildImageSharp;
}
export interface IExperience {
  company: string;
  description: string;
  position: string;
  timeFrame: string;
}

export interface Fluid {
  base64: string;
  aspectRatio: number;
  src: string;
  srcSet: string;
  sizes: string;
}

export interface DataJson {
  Nationality: string;
  description: string;
  language: string;
  location: string;
  title: string;
  fullName: string;
  education: IEducation[];
  experience: IExperience[];
  image: Image;
}

export interface IAboutQuery {
  dataJson: DataJson;
}

export interface IContactQueryNode {
  contact: string;
  title: string;
}

export interface IContactQueryEdge {
  node: IContactQueryNode;
}

export interface AllContactsJson {
  edges: IContactQueryEdge[];
}

export interface IContactQuery {
  allContactsJson: AllContactsJson;
}

export interface IStack {
  name: string;
}
