import { IStack } from './interfaces';

export const STACK: IStack[] = [
  { name: 'React.js' },
  { name: 'Typescript' },
  { name: 'Tailwind.css' },
  { name: 'Javascript' },
  { name: 'Next.js' },
  { name: 'Css' },
  { name: 'Scss' },
];
