import React from 'react';
import { navigate } from '@reach/router';
import Img from 'gatsby-image';
import { PreviewWrapper } from './projext-preview.style';
import { IEdge } from '../../ts/interfaces';
type ProjectPreviewProps = {
  data: IEdge;
};
const ProjectPreview = ({ data }: ProjectPreviewProps) => {
  const { slug, image, title, stack } = data.node.frontmatter;

  return (
    <PreviewWrapper>
      <div onClick={() => navigate(slug)} className='project'>
        <Img fluid={image.childImageSharp.fluid} alt={title} />
        <div className='info'>
          <h4>{title}</h4>
          <p>Stack: {stack.split(',').join(', ')}</p>
        </div>
      </div>
    </PreviewWrapper>
  );
};

export default ProjectPreview;
