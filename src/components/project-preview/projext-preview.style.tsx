import styled from 'styled-components';

export const PreviewWrapper = styled.div`
  color: white;
  display: flex;
  flex-direction: column;
  background-color: var(--sidebar-dark-color);
  border: 2px solid var(--border-color);
  border-radius: 8px 8px 0 0;
  overflow: clip;
  .project {
    cursor: pointer;
  }

  .info {
    display: flex;
    flex-direction: column;
    padding: 1rem;
    gap: 1rem;
    h4 {
      font-size: 16px;
      font-weight: 600;
    }
    p {
      color: gray;
      font-size: 14px;
    }
  }
`;
