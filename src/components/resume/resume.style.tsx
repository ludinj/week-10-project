import styled from 'styled-components';

export const ResumeStyle = styled.section`
  margin-top: 4rem;
  .resume-content {
    margin-bottom: 2rem;
  }
  .small-title {
    padding-bottom: 1rem;
  }

  .resume-content {
    border: 2px solid var(--border-color);
    padding: 0.5rem;
    background-color: var(--background-dark-color);
  }
`;
