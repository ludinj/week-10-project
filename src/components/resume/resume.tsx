import ResumeItem from '../resumeItem/resumeItem';
import React from 'react';
import { ResumeStyle } from './resume.style';
import SmallTitle from '../smalTitle/smallTitle';
import { IAboutQuery, IEducation, IExperience } from '../../ts/interfaces';
import EducationItem from '../educationItem/educationItem.';
type ResumeProps = {
  data: IAboutQuery;
};
const Resume = ({ data }: ResumeProps) => {
  const {
    fullName,
    image,
    description,
    education,
    experience,
    Nationality,
    language,
    location,
    title,
  } = data.dataJson;

  return (
    <ResumeStyle>
      <div className='small-title'>
        <SmallTitle title={'Working experience'} />
      </div>
      <div className='resume-content'>
        {experience.map((exp: IExperience, idx: number) => {
          return <ResumeItem experience={exp} key={idx} />;
        })}
      </div>

      <div className='small-title'>
        <SmallTitle title={'Education'} />
      </div>

      <div className='resume-content'>
        {education.map((edu: IEducation, idx: number) => {
          return <EducationItem education={edu} key={idx} />;
        })}
      </div>
    </ResumeStyle>
  );
};

export default Resume;
