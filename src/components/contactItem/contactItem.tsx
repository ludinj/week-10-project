import React from 'react';
import { ContactWrapper } from './contactItem.style';

type ContactItemProps = {
  title: string;
  contact: string;
};

const ContactItem = ({ title, contact }: ContactItemProps) => {
  return (
    <ContactWrapper>
      <div className='rigth-content'>
        <h6>{title}</h6>
        <p>{contact}</p>
      </div>
    </ContactWrapper>
  );
};

export default ContactItem;
