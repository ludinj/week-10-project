import React, { useRef } from 'react';
import { TitleStyle } from './title.style';

type TitleProps = {
  title: string;
};
function Title({ title }: TitleProps) {
  const sectionRef = useRef(null);

  return (
    <TitleStyle className='fadeIn'>
      <h2>{title}</h2>
    </TitleStyle>
  );
}

export default Title;
