import { SmallTitleStyle } from './smallTitle.style';
import React from 'react';

type SmallTitleProps = {
  title: string;
};
const SmallTitle = ({ title }: SmallTitleProps) => {
  return (
    <SmallTitleStyle>
      <h3>{title}</h3>
    </SmallTitleStyle>
  );
};

export default SmallTitle;
