import React from 'react';
import { IEducation } from '../../ts/interfaces';

import { ResumeItemStyle } from './educationItem.style';

type ResumeItemProps = {
  education: IEducation;
};

const EducationItem = ({ education }: ResumeItemProps) => {
  return (
    <ResumeItemStyle>
      <div className='left-content'>
        <p>{education.timeFrame}</p>
      </div>
      <div className='right-content'>
        <h5>{education.topic}</h5>
        <h6>{education.school}</h6>
      </div>
    </ResumeItemStyle>
  );
};

export default EducationItem;
