import { type } from 'os';
import React from 'react';
import { StyledButton } from './primaryButton.style';
import { navigate } from '@reach/router';

type PrimaryButtonProps = {
  title: string;
};
const PrimaryButton = ({ title }: PrimaryButtonProps) => {
  return (
    <StyledButton onClickCapture={() => navigate('/')}>{title}</StyledButton>
  );
};

export default PrimaryButton;
