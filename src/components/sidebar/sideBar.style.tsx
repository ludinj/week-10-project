import styled from 'styled-components';

export const Wrapper = styled.div`
  position: fixed;
  background-color: var(--sidebar-dark-color);
  width: 15rem;
  height: 100vh;
  transition: all 0.2s ease-in-out;
  z-index: 9;
  @media screen and (max-width: 900px) {
    transform: translateX(-100%);
    width: 11rem;
  }
  .toggle {
    position: absolute;
    right: -50px;
    top: 10px;
    display: none;
    color: var(--primary-color);
    border: 2px solid white;
    border-radius: 50%;
    cursor: pointer;
    @media screen and (max-width: 900px) {
      display: flex;
    }
  }
  &.show {
    transform: translateX(0);
    background-color: var(--sidebar-dark-color);
  }
`;
