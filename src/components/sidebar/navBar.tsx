import React from 'react';
import { Container } from './navbar.style';
import { Link } from 'gatsby';
import avatar from '../../images/avatar.jpg';
import { EPages } from '../../ts/enums';
const NavBar = () => {
  return (
    <Container>
      <div className='avatar'>
        <img src={avatar} alt='avatar' className='image' />
      </div>
      <div className='nav-items'>
        <Link to='/' activeClassName='active' className='nav-item'>
          About
        </Link>

        <Link
          to={`/${EPages.portfolio}`}
          activeClassName='active'
          className='nav-item'
        >
          Portfolio
        </Link>

        <Link
          to={`/${EPages.contact}`}
          activeClassName='active'
          className='nav-item'
        >
          Contact
        </Link>
      </div>
      <footer className='footer'>
        <p>@2022 Portfolio</p>
      </footer>
    </Container>
  );
};

export default NavBar;
