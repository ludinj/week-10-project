import React, { useState } from 'react';
import { Wrapper } from './sideBar.style';
import NavBar from './navBar';
import {
  BsFillArrowRightCircleFill,
  BsFillArrowLeftCircleFill,
} from 'react-icons/bs';

type SideBarProps = {
  navToggle: boolean;
  setNavToggle: React.Dispatch<React.SetStateAction<boolean>>;
};
const SideBar = () => {
  const [navToggle, setNavToggle] = useState(false);

  return (
    <Wrapper className={`${navToggle ? 'show' : ''}`}>
      {navToggle ? (
        <BsFillArrowLeftCircleFill
          size={40}
          className='toggle'
          onClick={() => setNavToggle((pre) => !pre)}
        />
      ) : (
        <BsFillArrowRightCircleFill
          size={40}
          className='toggle'
          onClick={() => setNavToggle((pre) => !pre)}
        />
      )}

      <NavBar />
    </Wrapper>
  );
};

export default SideBar;
