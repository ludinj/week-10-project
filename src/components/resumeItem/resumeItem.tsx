import React from 'react';
import { IExperience } from '../../ts/interfaces';
import { ResumeItemStyle } from './resumeItem.style';

type ResumeItemProps = {
  experience: IExperience;
};

const ResumeItem = ({ experience }: ResumeItemProps) => {
  return (
    <ResumeItemStyle>
      <div className='left-content'>
        <p>{experience.timeFrame}</p>
      </div>
      <div className='right-content'>
        <h5>{experience.position}</h5>
        <h6>{experience.company}</h6>
        <p>{experience.description}</p>
      </div>
    </ResumeItemStyle>
  );
};

export default ResumeItem;
