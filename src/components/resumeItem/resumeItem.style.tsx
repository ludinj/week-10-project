import styled from 'styled-components';

export const ResumeItemStyle = styled.div`
  display: flex;
  background-color: var(--background-dark-color);
  padding: 1rem;
  gap: 2rem;
  @media screen and (max-width: 480px) {
    flex-direction: column;
  }
  .left-content {
    width: 30%;
    padding-left: 0.5rem;
    p {
      padding-top: 0.3rem;
    }
  }
  .right-content {
    position: relative;
    padding-right: 1rem;
    width: 100%;

    h5 {
      font-size: 2rem;
      color: var(--primary-color);
      padding-bottom: 0.4rem;
    }
    h6 {
      padding-bottom: 0.6rem;
      color: var(--white-color);
      font-size: 1.2rem;
    }

    @media screen and (max-width: 780px) {
      h5 {
        font-size: 1.7rem;
      }
    }
    @media screen and (max-width: 460px) {
      p,
      h6 {
        font-size: 1rem;
      }
      h5 {
        font-size: 1.2rem;
      }
    }
  }
`;
