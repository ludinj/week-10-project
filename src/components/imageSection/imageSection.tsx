import React from 'react';
import { ImageWrapper } from './imageSection.style';
import { IAboutQuery } from '../../ts/interfaces';
import Img from 'gatsby-image';

type ImageSectionProps = {
  data: IAboutQuery;
};

const ImageSection = ({ data }: ImageSectionProps) => {
  const {
    fullName,
    image,
    description,
    education,
    experience,
    Nationality,
    language,
    location,
  } = data.dataJson;

  return (
    <ImageWrapper>
      <div className='left-content'>
        <Img
          fluid={image.childImageSharp.fluid}
          alt={fullName}
          className='bannerImage'
        />
      </div>
      <div className='rigth-content'>
        <h4>I am {fullName}</h4>
        <p className='paragraph'>{description}</p>
        <div className='about-info'>
          <div className='info-title'>
            <p>Full Name</p>
            <p>Nationality</p>
            <p>Language</p>
            <p>Location</p>
          </div>
          <div className='info'>
            <p>: {fullName}</p>
            <p>: {Nationality}</p>
            <p>: {language}</p>
            <p>: {location}</p>
          </div>
        </div>
      </div>
    </ImageWrapper>
  );
};

export default ImageSection;
