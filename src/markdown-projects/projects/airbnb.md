---
slug: /projects/airbnb
title: Airbnb clone
gitlab: https://github.com/ludinj/airbnb-clone
deploy: https://air-booking.vercel.app/
image: ../../images/airbnb-clone.png
description: A airbnb clone build with next.js you can login create new listings select your favorites and more
stack: Typescript,Tailwind.css,Next.js
---
