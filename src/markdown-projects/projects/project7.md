---
slug: /projects/project-7
title: Tick tack toe game.
gitlab: https://gitlab.com/ludinj/week-7-project
deploy: https://week-7-project.vercel.app/
image: ../../images/week7Image.png
description: A memory and tick tack toe game you, you can replay the game after a winner you can go to the previous/next  move and it shows when its a draw.
stack: Typescript,Scss,React.js
---
