---
slug: /projects/netflix-clone
title: Netflix clone
gitlab: https://github.com/ludinj/nextjs-netflix
deploy: https://nextjs-netflix-liard.vercel.app/
image: ../../images/netflix-clone.png
description: A netflix clone build with next.js tailwind.css and typescript
stack: Typescript,Tailwind.css,Next.js
---
