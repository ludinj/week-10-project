---
slug: /projects/project-2
title: Daily news site.
gitlab: https://gitlab.com/ludinj/week-2-project
deploy: https://week-2-project.vercel.app/
image: ../../images/week2Image.png
description: A site that displays the latest news, you can  search a story by name, filter then by language and change the number of new displayed
stack: Javascript,Css
---
