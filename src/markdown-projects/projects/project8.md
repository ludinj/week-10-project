---
slug: /projects/project-8
title: Marvel comics.
gitlab: https://gitlab.com/ludinj/week-8-project
deploy: https://week-8-project-5kaf.vercel.app/
image: ../../images/week8Image.png
description: A site where you can see the characters stories and comics from the marvel universe it has many filters to make the searching more convenient.
stack: Typescript,Scss,React.js
---
