---
slug: /projects/project-1
title: Validation form with konami code
gitlab: https://gitlab.com/ludinj/week-1-project
deploy: https://week-1-project.vercel.app/
image: ../../images/week1Image.png
description: A sing up form build with vanilla JS each field is validate with js and there is a secret konami code that displays an awesome gif (n,i,c,e,t,r,i,k).
stack: Javascript,Css
---
