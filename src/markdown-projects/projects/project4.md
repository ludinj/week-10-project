---
slug: /projects/project-4
title: Games site.
gitlab: https://gitlab.com/ludinj/week-4-project
deploy: https://week-4-project.vercel.app/
image: ../../images/week4Image.png
description: A site that displays games, you can paginate, Enter each game details page where you  check the game.
stack: Javascript,Scss,React.js
---
