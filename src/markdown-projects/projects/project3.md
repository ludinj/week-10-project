---
slug: /projects/project-3
title: Games characters site.
gitlab: https://gitlab.com/ludinj/week-3-project
deploy: https://week-3-project-ten.vercel.app/
image: ../../images/week3Image.png
description: A site that displays games characters, you can search search  by name, filter then by  tag and enter the details page of each character where you can like the post, comment or edit the post.
stack: Javascript,Css
---
