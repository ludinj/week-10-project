import styled from 'styled-components';

export const ProjectWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 2rem;
  gap: 2rem;
  color: white;
  padding: 2rem;
  border: 1px solid var(--border-color);
  background-color: var(--sidebar-dark-color);
  @media screen and (max-width: 900px) {
    padding: 1rem;
  }
  .links {
    display: flex;
    width: 100%;
    justify-content: space-between;

    @media screen and (max-width: 900px) {
      flex-direction: column;
      gap: 1rem;
    }
    svg {
      font-size: 25px;
    }

    div {
      display: flex;
      align-items: center;
      gap: 1rem;
    }
  }
`;
