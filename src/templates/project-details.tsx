import React from 'react';
import Img from 'gatsby-image';
import { graphql } from 'gatsby';
import { ProjectWrapper } from './projects-details.styles';
import { AiFillGitlab } from 'react-icons/ai';
import { SiVercel } from 'react-icons/si';
import '../styles/global.css';
type ProjectDetailsProps = {
  data: any;
};
const ProjectDetails = ({ data }: ProjectDetailsProps) => {
  const { html } = data.markdownRemark;
  const { title, gitlab, description, image, stack, deploy } =
    data.markdownRemark.frontmatter;

  return (
    <ProjectWrapper>
      <h2>{title}</h2>
      <div className='image'>
        <Img fluid={image.childImageSharp.fluid} alt={title} />
      </div>
      <p>{description}</p>
      <h3>Stack: {stack}</h3>
      <div className='links'>
        <div>
          <h3>Visit the repository:</h3>
          <a href={`${gitlab}`} target='_blank' rel='noopener noreferrer'>
            <AiFillGitlab />
          </a>
        </div>
        <div>
          <h3>Deployment link:</h3>
          <a href={`${deploy}`} target='_blank' rel='noopener noreferrer'>
            <SiVercel />
          </a>
        </div>
      </div>
      <div dangerouslySetInnerHTML={{ __html: html }} />
    </ProjectWrapper>
  );
};

export const query = graphql`
  query PostQuery($slug: String) {
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        image {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
        stack
        description
        gitlab
        deploy
      }
    }
  }
`;

export default ProjectDetails;
