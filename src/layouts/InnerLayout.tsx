import styled from 'styled-components';

export const InnerLayout = styled.div`
  padding: 4rem;
  margin-left: 15rem;
  @media screen and (max-width: 900px) {
    margin-left: 0;
    padding: 4rem 1rem;
  }
`;
