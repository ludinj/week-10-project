import React from 'react';
import Sidebar from '../components/sidebar/sideBar';
import { InnerLayout } from './InnerLayout';
type LayoutProps = {
  children: React.ReactNode;
};

const MainLayout = ({ children }: LayoutProps) => {
  return (
    <div>
      <Sidebar />
      <InnerLayout>{children}</InnerLayout>
    </div>
  );
};

export default MainLayout;
