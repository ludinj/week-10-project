import * as React from 'react';
import type { HeadFC } from 'gatsby';
import '../styles/global.css';
import ImageSection from '../components/imageSection/imageSection';
import Resume from '../components/resume/resume';
import { graphql } from 'gatsby';
import { IAboutQuery } from '../ts/interfaces';

type IndexPageProps = {
  data: IAboutQuery;
};
const IndexPage = ({ data }: IndexPageProps) => {
  return (
    <div>
      <ImageSection data={data} />
      <Resume data={data} />
    </div>
  );
};

export const query = graphql`
  query AboutQuery {
    dataJson {
      Nationality
      description
      language
      location
      fullName
      education {
        school
        timeFrame
        topic
      }
      experience {
        company
        description
        position
        timeFrame
      }
      image {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
`;

export default IndexPage;

export const Head: HeadFC = () => <title>Home Page</title>;
