import React from 'react';
import ContactItem from '../components/contactItem/contactItem';
import Title from '../components/title/title';
import { ContactWrapper, Container } from '../styles/contact.style';
import { graphql } from 'gatsby';
import { IContactQuery, IContactQueryEdge } from '../ts/interfaces';
import '../styles/global.css';

type contactProps = {
  data: IContactQuery;
};
const contact = ({ data }: contactProps) => {
  const edges = data.allContactsJson.edges;
  return (
    <Container>
      <Title title={'Contact information.'} />
      <ContactWrapper>
        {edges.map((edge: IContactQueryEdge, idx: number) => {
          return (
            <ContactItem
              title={edge.node.title}
              contact={edge.node.contact}
              key={idx}
            />
          );
        })}
      </ContactWrapper>
    </Container>
  );
};
export const query = graphql`
  query ContactQuery {
    allContactsJson {
      edges {
        node {
          contact
          title
        }
      }
    }
  }
`;
export default contact;
