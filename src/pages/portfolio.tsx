import React, { useState } from 'react';
import { graphql } from 'gatsby';
import { InnerLayout } from '../layouts/InnerLayout';
import ProjectPreview from '../components/project-preview/project-preview';
import { Grid, Filter } from '../styles/portfolio.style';
import '../styles/global.css';
import Title from '../components/title/title';
import { IEdge, IQuery } from '../ts/interfaces';
import { STACK } from '../ts/constants';
type PortProps = {
  data: IQuery;
};
const Portfolio = ({ data }: PortProps) => {
  const edges = data.allMarkdownRemark.edges;
  const [projects, setProjects] = useState<IEdge[]>(edges);

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const value = e.target.value;
    const currentProjects: IEdge[] = [];

    if (!value) {
      setProjects(edges);
      return;
    }

    edges.forEach((edge: IEdge) => {
      const { stack } = edge.node.frontmatter;
      const stackArray = stack.split(',');
      stackArray.forEach((stack: string) => {
        if (stack === value) {
          currentProjects.push(edge);
        }
      });
      setProjects(currentProjects);
    });
  };
  return (
    <div>
      <Title title='Portfolio.' />
      <Filter>
        <label>Filter by stack:</label>
        <select name='stack' id='stack' onChange={(e) => handleChange(e)}>
          <option value=''>All</option>
          {STACK.map((stack) => {
            return <option value={stack.name}>{stack.name}</option>;
          })}
        </select>
      </Filter>
      <Grid>
        {projects.map((edge: any, idx: number) => {
          return <ProjectPreview data={edge} key={idx} />;
        })}
      </Grid>
    </div>
  );
};
export const query = graphql`
  query MyQuery {
    allMarkdownRemark {
      edges {
        node {
          frontmatter {
            title
            image {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            stack
            slug
            gitlab
            deploy
          }
        }
      }
    }
  }
`;
export default Portfolio;
