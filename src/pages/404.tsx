import * as React from 'react';
import { Link, HeadFC } from 'gatsby';
import { NoPageFoundWrapper } from '../styles/404.styles';
import PrimaryButton from '../components/primaryBottom/primaryButton';
import '../styles/global.css';
const NotFoundPage = () => {
  return (
    <NoPageFoundWrapper>
      <h1>Oops!</h1>
      <h2>404- PAGE NOT FOUND.</h2>
      <p>
        The page you are looking for might have been removed had its name
        changed or is temporarily unavailable.
      </p>

      <PrimaryButton title='Go back' />
    </NoPageFoundWrapper>
  );
};

export default NotFoundPage;

export const Head: HeadFC = () => <title>Not found</title>;
