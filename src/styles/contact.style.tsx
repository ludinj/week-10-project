import styled from 'styled-components';

export const ContactWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(350px, 1fr));
  gap: 2rem;
  margin-top: 2rem;
  margin-bottom: 2rem;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;
