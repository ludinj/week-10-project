import styled from 'styled-components';

export const NoPageFoundWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  height: 100vh;
  gap: 2rem;
  h1 {
    font-size: 80px;
  }
`;
