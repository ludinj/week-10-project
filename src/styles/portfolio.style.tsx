import styled from 'styled-components';

export const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(340px, 1fr));
  grid-gap: 3rem 1rem;
  margin-top: 2rem;
  margin-bottom: 2rem;
`;

export const Title = styled.div`
  h1 {
    font-size: 28px;
    color: white;
    position: relative;

    &::before {
      content: '';
      position: absolute;
      bottom: -10px;
      width: 7.4rem;
      height: 0.3rem;
      background-color: var(--background-light-color-2);
      border-radius: 15px;
      left: 0;
    }
    &::after {
      content: '';
      position: absolute;
      bottom: -10px;

      width: 4rem;
      height: 0.3rem;
      background-color: var(--primary-color-light);
      border-radius: 15px;
      left: 0;
    }
  }
`;

export const Filter = styled.div`
  display: flex;
  margin-top: 2rem;
  color: white;
  gap: 1rem;
`;
