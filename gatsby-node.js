const path = require('path');
exports.createPages = async function ({ graphql, actions }) {
  const { data } = await graphql(`
    query Projects {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              slug
            }
          }
        }
      }
    }
  `);

  data.allMarkdownRemark.edges.forEach((edge) => {
    actions.createPage({
      path: edge.node.frontmatter.slug,
      component: path.resolve('./src/templates/project-details.tsx'),
      context: { slug: edge.node.frontmatter.slug },
    });
  });
};
